package com.tom.domain;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import com.tom.cache.CacheElement;

public class PRIMEInstrumentTest {

    @Test public void testMergeInto() {
        
        CacheElement cache = new CacheElement();
        BaseInstrument lmeMessage = new LMEInstrument(LocalDate.of(2018, 3, 15), LocalDate.of(2018, 3, 17), "LME_PB", "Lead 13 March 2018", "PB_03_2018");
        lmeMessage.mergeInto(cache);
        BaseInstrument primeMessage = new PRIMEInstrument(LocalDate.of(2018, 3, 14), LocalDate.of(2018, 3, 18), "LME_PB", "Lead 13 March 2018", "PB_03_2018", false);
        primeMessage.mergeInto(cache);
        
        
        Assert.assertEquals("2018-03-15", cache.getLastTradingDate().toString());
        Assert.assertEquals("2018-03-17", cache.getDeliveryDate().toString());
        Assert.assertEquals("PB", cache.getMarket());
        Assert.assertEquals("Lead 13 March 2018", cache.getLabel());
        Assert.assertFalse(cache.isTradable());
    }

}
