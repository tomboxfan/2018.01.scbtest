package com.tom.cache;

import java.time.LocalDate;

/**
 * Instrument image cached in the application.
 */
public class CacheElement {
    
    private LocalDate lastTradingDate;
    private boolean isLMELastTradingDate = false;
    
    private LocalDate deliveryDate;
    private boolean isLMEDeliveryDate = false;
            
    private String market;
    private String label;
    
    private boolean tradable = true; // default to TRUE

    // -- auto-generated getter and setter, please ignore ---------------------

    public boolean isLMELastTradingDate() {
        return isLMELastTradingDate;
    }

    public void setLMELastTradingDate(boolean isLMELastTradingDate) {
        this.isLMELastTradingDate = isLMELastTradingDate;
    }

    public boolean isLMEDeliveryDate() {
        return isLMEDeliveryDate;
    }

    public void setLMEDeliveryDate(boolean isLMEDeliveryDate) {
        this.isLMEDeliveryDate = isLMEDeliveryDate;
    }
    
    public LocalDate getLastTradingDate() {
        return lastTradingDate;
    }

    public void setLastTradingDate(LocalDate lastTradingDate) {
        this.lastTradingDate = lastTradingDate;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    public boolean isTradable() {
        return tradable;
    }

    public void setTradable(boolean tradable) {
        this.tradable = tradable;
    }

    //publish to console
    public void publish() {
        System.out.println("|  LAST_TRADING_DATE             | DELIVERY_DATE    |  MARKET         | LABEL                      | TRADABLE   |");
        System.out.println(String.format("|  %s                    | %s       |  %s             | %s         |  %s      |", lastTradingDate, deliveryDate, market, label, tradable));   
        System.out.println();
    }
}
