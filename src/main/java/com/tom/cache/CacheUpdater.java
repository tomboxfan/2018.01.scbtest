package com.tom.cache;

import java.util.concurrent.LinkedBlockingQueue;

import com.tom.domain.BaseInstrument;

/**
 * CacheUpdater queue all the cache update request in a line, as long as they are for the same instrument.
 */
public class CacheUpdater extends Thread {

    private CacheElement cache;
    private LinkedBlockingQueue<BaseInstrument> queue;

    public CacheUpdater() {
        cache = new CacheElement();
        queue = new LinkedBlockingQueue<>();
    }
    
    public void addMessage(BaseInstrument message) throws InterruptedException {
        queue.put(message);
    }

    @Override public void run() {
        try {
            while(true) {
                BaseInstrument instrument = queue.take(); //most of the time, the thread is in blocked status, unless a msg is put in the queue. 
                instrument.mergeInto(cache);
                cache.publish();
            }
        }
        catch (InterruptedException e) {
            System.out.println("Blocking interrupted");
        }
    }
}
