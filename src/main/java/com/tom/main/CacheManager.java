package com.tom.main;

import java.util.concurrent.ConcurrentHashMap;

import com.tom.cache.CacheUpdater;
import com.tom.domain.BaseInstrument;

/**
 * Wrapper of a ConcurrentHashMap.
 * 
 * Each instrument is mapped to a thread - CacheUpdater.
 * Though we may have many threads, but most of the time they are in blocked status if the messages flow in not that fast.
 * 
 * On the other hand, if messages flow in super fast, then we have to create an ExecutorService to manage 12 threads.
 */
public class CacheManager {
    
    private ConcurrentHashMap<String, CacheUpdater> map = new ConcurrentHashMap<>();
    
    //lock free thread-safe method.
    public void receiveMessage(BaseInstrument message) throws InterruptedException {
        
        CacheUpdater oldCacheUpdater = map.putIfAbsent(message.getKeyCode(), new CacheUpdater());
        
        if(oldCacheUpdater == null) {
            CacheUpdater cacheUpdater = map.get(message.getKeyCode());
            cacheUpdater.addMessage(message);
            cacheUpdater.start();
        }
        else {
            oldCacheUpdater.addMessage(message);
        }
    }
}
