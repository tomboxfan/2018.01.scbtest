package com.tom.main;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

import com.tom.domain.BaseInstrument;
import com.tom.domain.LMEInstrument;
import com.tom.domain.PRIMEInstrument;

public class Main {
    public static void main(String args[]) throws InterruptedException {
        
        CacheManager cacheManager = new CacheManager();
        
        //In reality, we can have multiple threads, sending messages to CacheManager concurrently
        
        //Step 1) LME publishes PB_03_2018
        BaseInstrument lmeMessage = new LMEInstrument(LocalDate.of(2018, 3, 15), LocalDate.of(2018, 3, 17), "LME_PB", "Lead 13 March 2018", "PB_03_2018");
        cacheManager.receiveMessage(lmeMessage);
        
        //Step 2) LME publishes PRIME_PB_03_2018
        BaseInstrument primeMessage = new PRIMEInstrument(LocalDate.of(2018, 3, 14), LocalDate.of(2018, 3, 18), "LME_PB", "Lead 13 March 2018", "PB_03_2018", false);
        cacheManager.receiveMessage(primeMessage);
        
        TimeUnit.SECONDS.sleep(5);
        System.exit(0);
    }
}
