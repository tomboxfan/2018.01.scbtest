package com.tom.domain;

import java.time.LocalDate;

import com.tom.cache.CacheElement;

/**
 * BaseInstrument stands for an Instrument published from some source (LME / PRIME).
 */
public abstract class BaseInstrument {
    
    //common fields for all sources.
    LocalDate lastTradingDate;
    LocalDate deliveryDate;
    String market;
    String label;
    
    /**
     * Eache source(LME/PRIME) use different field for keycode.
     * LME uses field code
     * PRIME uses field exchangeCode
     */
    abstract public String getKeyCode();
    
    /**
     * Each source(LME/PRIME) should know how to merge itself into the application cache.
     * For new source in the future, we can simply implement this method for its merging rules.
     */
    abstract public void mergeInto(CacheElement cache);
    
    // -- auto-generated getter and setter, please ignore ---------------------
    
    public LocalDate getLastTradingDate() {
        return lastTradingDate;
    }
    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }
    public String getMarket() {
        return market;
    }
    public String getLabel() {
        return label;
    }
}
