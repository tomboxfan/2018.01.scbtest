package com.tom.domain;

import java.time.LocalDate;

import com.tom.cache.CacheElement;

/**
 * Instrument published from LME.
 * 
 * LME instrument doesn't have field - tradable, and its id field name is 'code'. (requirment.txt line 72)
 */
public class LMEInstrument extends BaseInstrument {
    
    private String code;
    
    public LMEInstrument(LocalDate lastTradingDate, LocalDate deliveryDate, String market, String label, String code) {
        this.lastTradingDate = lastTradingDate;
        this.deliveryDate = deliveryDate;
        this.market = market;
        this.label = label;
        this.code = code;
    }
    
    @Override public String getKeyCode() {
        return this.code;
    }
    
    /**
     * We trust/use the last trading date and delivery date from the LME exchange over that of PRIME. 
     */
    @Override public void mergeInto(CacheElement cache) {
        
        //merge last trading date ---------------
        if(lastTradingDate != null) {
            cache.setLastTradingDate(lastTradingDate);
            cache.setLMELastTradingDate(true);
        }
        
        //merge delivery date -------------------
        if(deliveryDate != null) {
            cache.setDeliveryDate(deliveryDate);
            cache.setLMEDeliveryDate(true);
        }
        
        //merge market ---------------------------
        if("LME_PB".equalsIgnoreCase(market)) {
            cache.setMarket("PB");
        }
        else {
            cache.setMarket(market);
        }

        //merge label -----------------------------
        cache.setLabel(label);
    }
    

    // -- auto-generated getter and setter, please ignore ---------------------
    
    public String getCode() {
        return code;
    }
}
