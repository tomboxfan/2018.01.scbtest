package com.tom.domain;

import java.time.LocalDate;

import com.tom.cache.CacheElement;

/**
 * Instrument published from Prime.
 * 
 * Prime instrument has field - tradable, and its id field name is 'exchangeCode'. (requirment.txt line 72)
 */
public class PRIMEInstrument extends BaseInstrument {
    
    private String exchangeCode;
    private boolean tradable;

    public PRIMEInstrument(LocalDate lastTradingDate, LocalDate deliveryDate, String market, String label, String exchangeCode, boolean tradable) {
        this.lastTradingDate = lastTradingDate;
        this.deliveryDate = deliveryDate;
        this.market = market;
        this.label = label;
        this.exchangeCode = exchangeCode;
        this.tradable = tradable;
    }
    
    @Override public String getKeyCode() {
        return exchangeCode;
    }    

    @Override public void mergeInto(CacheElement cache) {
        
        //merge last trading date ---------------
        if(lastTradingDate != null && !cache.isLMELastTradingDate()) {
            cache.setLastTradingDate(lastTradingDate);
        }
        
        //merge delivery date -------------------
        if(deliveryDate != null && !cache.isLMEDeliveryDate()) {
            cache.setDeliveryDate(deliveryDate);
        }
        
        //merge market ---------------------------
        if("LME_PB".equalsIgnoreCase(market)) {
            cache.setMarket("PB");
        }
        else {
            cache.setMarket(market);
        }

        //merge label -----------------------------
        cache.setLabel(label);
        cache.setTradable(tradable);
    }
    
    
    // -- auto-generated getter and setter, please ignore ---------------------
    
    public String getExchangeCode() {
        return exchangeCode;
    }
    public boolean isTradable() {
        return tradable;
    }
}
